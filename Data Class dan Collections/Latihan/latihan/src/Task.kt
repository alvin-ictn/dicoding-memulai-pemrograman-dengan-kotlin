fun main() {

    // TODO 1

    val vehicle = mapOf("Type" to "Motorcycle",
            "Maximal Speed" to "230Km/s",
            "Maximal Tank" to "10Ltr")

    // TODO 2
    val type = vehicle["Type"]
    val maxSpeed = vehicle["Maximal Speed"]
    val maxTank = vehicle["Maximal Tank"]

    // TODO 3
    println("""
Vehicle
Type: $type
Maximal Speed: $maxSpeed
Maximal Tank: $maxTank
    """.trimIndent())
}

