// main function
fun main() {
    val name = "Alvin"

    print("Hello my name is ")
    println(name)
    print(if (true) "Always true" else "Always false")
}
/*
   output:
       Hello my name is Alvin
       Always true
*/