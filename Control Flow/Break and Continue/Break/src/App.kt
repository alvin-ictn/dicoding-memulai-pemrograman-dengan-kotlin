// main function
fun main() {
    val listOfInt = listOf(1, 2, 3, 4, 5, null, 7)

    for (i in listOfInt) {
        if (i == null) break
        print(i)
    }
}